﻿using botya.web.Configuration;
using botya.web.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace botya.web.Services
{
    public class BotService
    {
        private BotOptions _botOptions;

        private TelegramBotClient _client;
        public BotService(IOptions<BotOptions> options)
        {
            _botOptions = options.Value;
            _client = new TelegramBotClient(_botOptions.Token);
        }

        public async Task EchoAsync(Update update)
        {
            if (update.Type != UpdateType.Message)
                return;

            var message = update.Message;

            switch (message.Type)
            {
                case MessageType.Text:
                    // Echo each Message
                    await SwitchCommand(message);
                    break;

            }
        }

        public async Task SwitchCommand(Message message)
        {
            switch (message.Text)
            {
                case BotCommands.SA:
                    await SendSA();
                    break;

                default:
                    await _client.SendTextMessageAsync(message.Chat.Id, message.Text);
                    break;

            }
        }

        public async Task SendSA()
        {
            using (var client = new HttpClient())
            {
                var message = new HttpRequestMessage()
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri("http://localhost:443?Command=SA")
                };

                await client.SendAsync(message);
            }
        }
    }
}
