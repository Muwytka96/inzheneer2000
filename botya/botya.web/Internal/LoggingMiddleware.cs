﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace botya.web.Internal
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public LoggingMiddleware(RequestDelegate next,
                                                ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<LoggingMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            //await LogRequest(context);
            //code dealing with the request
            await _next(context);
            //code dealing with the response

            //await LogResponse(context);


        }

        private async Task LogRequest(HttpContext context)
        {
            ////using (var stream = new StreamReader(context.Request.Body))
            ////{
            ////    try
            ////    {
            ////        var body = stream.ReadToEndAsync();

            ////        _logger.LogInformation($"Body: {body}");
            ////    }
            ////    catch (Exception ex)
            ////    {
            ////        var e = ex;
            ////    }
            ////}
        }

        private async Task LogResponse(HttpContext context)
        {
            ////using (var stream = new StreamReader(context.Response.Body))
            ////{
            ////    try
            ////    {
            ////        var body = stream.ReadToEndAsync();

            ////        _logger.LogInformation($"Body: {body}");
            ////    }
            ////    catch (Exception ex)
            ////    {
            ////        var e = ex;
            ////    }
            ////}
        }
    }
}
