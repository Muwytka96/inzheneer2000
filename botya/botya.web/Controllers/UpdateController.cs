﻿using botya.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace botya.web.Controllers
{
    [Route("api")]
    [ApiController]
    public class UpdateController : ControllerBase
    {
        private readonly BotService _botService;

        public UpdateController(BotService botService)
        {
            _botService = botService;
        }

        [HttpPost("update")]
        public async Task<IActionResult> Post(
            [FromBody] Update update)
        {
            //Check Password this


            await _botService.EchoAsync(update);

            return Ok();
        }
    }
}
